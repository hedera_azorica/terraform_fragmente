terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {
  host = "npipe:////.//pipe//docker_engine"
}

# Crear la red de Docker
resource "docker_network" "bec_logs" {
  name = "bec_logs"
}

# Descargar imagen de Grafana
resource "docker_image" "grafana" {
  name         = "grafana/grafana:latest"
  keep_locally = true
}

# Descargar imagen de Loki
resource "docker_image" "loki" {
  name         = "grafana/loki:latest"
  keep_locally = true
}

# Descargar imagen de Promtail
resource "docker_image" "promtail" {
  name         = "grafana/promtail:latest"
  keep_locally = true
}

# Contenedor de Grafana
resource "docker_container" "grafana" {
  image = docker_image.grafana.name
  name  = "grafana"
  ports {
    internal = 3000
    external = 3000
  }
  network_mode = docker_network.bec_logs.name
}

# Contenedor de Loki
resource "docker_container" "loki" {
  image = docker_image.loki.name
  name  = "loki"
  ports {
    internal = 3100
    external = 3100
  }
  command = [
    "--config.file=/etc/loki/local-config.yaml"
  ]
  volumes {
    host_path      = "C:\\Users\\evargasm\\Documents\\git\\terraform_fragmente\\logs.me\\config\\loki-config.yaml"
    container_path = "/etc/loki/local-config.yaml"
  }
  network_mode = docker_network.bec_logs.name
}

# Contenedor de Promtail
resource "docker_container" "promtail" {
  image = docker_image.promtail.name
  name  = "promtail"
  command = [
    "--config.file=/etc/promtail/promtail-config.yaml"
  ]
  volumes {
    host_path      = "C:\\Users\\evargasm\\Documents\\git\\terraform_fragmente\\logs.me\\config\\promtail-config.yaml"
    container_path = "/etc/promtail/promtail-config.yaml"
  }
  volumes {
    host_path      = "D:\\debug.zone\\08.08.2024"
    container_path = "/mnt/logs"
  }
  network_mode = docker_network.bec_logs.name
}
