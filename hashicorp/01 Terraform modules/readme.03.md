# ¿Qué es un módulo de Terraform?

Un módulo de Terraform es un conjunto de archivos de configuración ubicados en un único directorio. Incluso la configuración más simple con un solo archivo `.tf` se considera un módulo. El directorio donde ejecutas los comandos de Terraform se llama "módulo raíz".

Por ejemplo, un módulo simple podría tener esta estructura:

-------------------
📄 **LICENSE**
-------------------
📄 **README.md**
-------------------
📄 **main.tf**
-------------------
📄 **variables.tf**
-------------------
📄 **outputs.tf**
-------------------

## Llamada y uso de módulos

Terraform usa los archivos de configuración en un directorio, pero también puede llamar a otros módulos en diferentes directorios a través de bloques de módulo. Estos módulos llamados se conocen como "módulos hijos".

## Módulos locales y remotos

Los módulos pueden cargarse desde el sistema de archivos local o desde fuentes remotas como el Registro de Terraform, sistemas de control de versiones, URLs HTTP, y registros privados como HCP Terraform o Terraform Enterprise.
