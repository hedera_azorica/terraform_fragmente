# Mejores prácticas para módulos en Terraform

Los módulos de Terraform son similares a bibliotecas o paquetes en lenguajes de programación y ofrecen beneficios similares. Se recomienda seguir estas prácticas:

- **Nombra tu proveedor**: Usa la convención `terraform-<PROVIDER>-<NOMBRE>` para poder publicar en los registros de HCP Terraform o Terraform Enterprise.

- **Diseña con módulos en mente**: Incluso para configuraciones moderadamente complejas, los beneficios de usar módulos superan el tiempo necesario para implementarlos.

- **Organiza y encapsula con módulos locales**: Organizar tu configuración en módulos desde el principio reduce la carga de mantenimiento a medida que la infraestructura crece.

- **Utiliza el Registro Público de Terraform**: Encuentra módulos útiles para implementar configuraciones comunes de manera más rápida y confiable.

- **Publica y comparte módulos**: Los módulos facilitan la colaboración en equipo en la creación y mantenimiento de la infraestructura.
