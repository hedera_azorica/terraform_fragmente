# Configuración de ejemplo en Terraform

La configuración de ejemplo en Terraform establece dos argumentos clave: `source` y `version`.

- **source**: Es obligatorio y define de dónde se cargará el módulo, ya sea del Registro de Terraform, una URL, o un módulo local.

- **version**: No es obligatorio, pero se recomienda especificarlo para controlar qué versión del módulo se carga. Sin este argumento, se cargará la última versión disponible.

Otros argumentos en los bloques de módulo son tratados como variables de entrada para el módulo.
