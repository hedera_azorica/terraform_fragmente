terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  profile = "cardigan-user"
  region  = "us-east-2"
}

resource "aws_instance" "app_server" {
  ami           = "ami-0532b75a38d817c74"
  instance_type = "t4g.nano"
  tags = {
    Name = "ExampleAppServerInstance"
  }
}
