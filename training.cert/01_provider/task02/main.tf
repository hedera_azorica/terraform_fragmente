provider "aws" {
  profile = "cardigan-user"
  region  = "us-west-2"
}

resource "aws_s3_bucket" "task02-bucket-s3" {
  count  = 6
  bucket = "task02-s3-${random_string.randon_prefix[count.index].id}"
}
