resource "random_string" "randon_prefix" {
  count   = 6
  length  = 10
  special = false
  numeric = false
  upper   = false
}
