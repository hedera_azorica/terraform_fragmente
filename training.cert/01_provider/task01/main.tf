resource "local_file" "evil_corp_admin" {
  count    = 5
  content  = base64encode("${random_string.randon_prefix[count.index].id}")
  filename = "evil-admin-${random_string.randon_prefix[count.index].id}.txt"
}

resource "local_file" "evil_corp_seo" {
  count    = 5
  content  = base64encode("${random_string.randon_prefix[count.index].id}")
  filename = "evil-seo-${random_string.randon_prefix[count.index].id}.txt"
}
