# proveedor de docker : https://registry.terraform.io/providers/kreuzwerker/docker/latest

terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host = "npipe:////.//pipe//docker_engine"
}

resource "docker_image" "recurso_imagen_nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "recurso_container_nginx" {
  image = docker_image.recurso_imagen_nginx.image_id
  name  = "name_custom_container_nginx"
  ports {
    internal = 80
    external = 8080
  }
}
