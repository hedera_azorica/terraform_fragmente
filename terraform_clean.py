import os
import shutil
import errno
import time
import stat

def handle_remove_readonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.remove, os.unlink) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IWRITE)
        func(path)
    else:
        raise

def delete_directory(directory):
    try:
        if os.path.exists(directory):
            shutil.rmtree(directory, onerror=handle_remove_readonly)
            print(f"  [✔] Carpeta .terraform eliminada: {os.path.relpath(directory)}")
        else:
            print(f"  [✘] La carpeta no existe: {os.path.relpath(directory)}")
    except Exception as e:
        print(f"  [✘] Error al intentar eliminar {os.path.relpath(directory)}: {e}")

def main():
    # Encabezado del script
    print("="*50)
    print("        Terraform Cleaner - Eliminación Recursiva")
    print("="*50)

    # Directorio actual desde donde se ejecuta el script
    root_dir = os.getcwd()
    print(f"\nDirectorio actual: {root_dir}\n")
    
    # Encontrar carpetas .terraform hasta en 3 niveles de profundidad
    terraform_dirs = []
    for dirpath, dirnames, _ in os.walk(root_dir):
        # Calcular la profundidad del directorio actual
        depth = dirpath[len(root_dir):].count(os.sep)
        if depth > 3:
            continue
        if '.terraform' in dirnames:
            terraform_dirs.append(os.path.join(dirpath, '.terraform'))

    if len(terraform_dirs) == 0:
        print("No se encontraron carpetas .terraform para eliminar.")
        print("="*50)
        return

    # Iniciar el borrado
    print("\nIniciando el borrado...\n")
    start_time = time.time()

    for directory in terraform_dirs:
        delete_directory(directory)
    
    end_time = time.time()
    elapsed_time = end_time - start_time

    print("\nBorrado completado.")
    print(f"Tiempo total: {elapsed_time:.2f} segundos")
    print("="*50)

if __name__ == "__main__":
    main()
